﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<Link> objctFromCookies = new List<Link>();
            if (Request.Cookies["history"] != null) {
                if (Request.Cookies["history"]["links"] != null) {
                    string storedLinksInCookie;
                    storedLinksInCookie = Request.Cookies["history"]["links"];
                    objctFromCookies = JsonConvert.DeserializeObject<List<Link>>(storedLinksInCookie);
                    foreach (Link link in objctFromCookies)
                    {
                        link.fullLink = Uri.UnescapeDataString(link.fullLink);
                    }
                }
            }
                return View(objctFromCookies);
        }

        public ActionResult ShortLink(String inputLink) {
            var builder = new UriBuilder(inputLink);
                var test = Uri.EscapeDataString(builder.Uri.AbsoluteUri);


            HttpCookie historyCookies = new HttpCookie("history");
            List<Link> objctFromCookies = new List<Link>();
            String storedLinksInCookie = "";
            int maxId = 0;
            if (Request.Cookies["history"] != null) { 
                if (Request.Cookies["history"]["links"] != null) {
                    storedLinksInCookie = Request.Cookies["history"]["links"];
                    objctFromCookies = JsonConvert.DeserializeObject<List<Link>>(storedLinksInCookie);
                }
                if (Request.Cookies["history"]["maxId"] != null) {
                    maxId = Int32.Parse(Request.Cookies["history"]["maxId"]);
                }
            }
            if (!(storedLinksInCookie.Contains(test))) {
                Link newlink = new Link {
                    id = maxId + 1,
                    fullLink = test,
                    shortLink = HttpContext.Request.Url.Authority + "/"+ "shlink" + "?id=" + (int)(maxId+1),
                    clickCount = 0
                };
                maxId = newlink.id;
                objctFromCookies.Add(newlink);
                storedLinksInCookie = JsonConvert.SerializeObject(objctFromCookies);
            }
            historyCookies["maxId"] = maxId.ToString();
            historyCookies["links"] = storedLinksInCookie;
            historyCookies.Expires = DateTime.Now.AddDays(1d);
            Response.Cookies.Add(historyCookies);
            System.Threading.Thread.Sleep(5000);


            return RedirectToAction("Index");
        }
        [Route("shlink")]

        public ActionResult IncrementClickCountForSelectedId(int id)
        {
            List<Link> objctFromCookies = new List<Link>();
            int maxId = 0;
            Link currentLink = null;
            if (Request.Cookies["history"] != null) {
                if (Request.Cookies["history"]["links"] != null) {
                    string storedLinksInCookie;
                    storedLinksInCookie = Request.Cookies["history"]["links"];
                    objctFromCookies = JsonConvert.DeserializeObject<List<Link>>(storedLinksInCookie);
                    foreach (Link link in objctFromCookies) {
                        if (link.id == id) {
                            link.clickCount++;
                            currentLink = link;
                        }
                    }
                    HttpCookie historyCookies = new HttpCookie("history");
                    historyCookies["maxId"] = maxId.ToString();
                    storedLinksInCookie = JsonConvert.SerializeObject(objctFromCookies);
                    historyCookies["links"] = storedLinksInCookie;
                    Response.Cookies.Add(historyCookies);
                }
                if (Request.Cookies["history"]["maxId"] != null) {
                    maxId = Int32.Parse(Request.Cookies["history"]["maxId"]);
                }
            }
            return Redirect(Uri.UnescapeDataString(currentLink.fullLink));

        }
    }
}