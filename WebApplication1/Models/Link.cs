﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Link
    {
        public int id { get; set; }
        public String fullLink { get; set; }
        public string shortLink { get; set; }
        public int clickCount { get; set; }


    }
}